package com.artoconnect.whatgroups.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.fragment.GroupListFragment;


/**
 * Created by Christian Tamakloe on 20/02/2016.
 */
public class HomeScreenPagerAdapter extends FragmentStatePagerAdapter {

    final int PAGE_COUNT = 3;
    private Context mContext;
    private Integer[] mTabTitles =
            new Integer[]{R.string.title_recent, R.string.title_joined, R.string.title_created};

    public HomeScreenPagerAdapter(FragmentManager supportFragmentManager, Context context) {
        super(supportFragmentManager);
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return GroupListFragment.newInstance(GroupListFragment.ALL);
            case 1:
                return GroupListFragment.newInstance(GroupListFragment.REQUESTS);
            case 2:
                return GroupListFragment.newInstance(GroupListFragment.ADDED);
            default:
                return GroupListFragment.newInstance(GroupListFragment.ALL);
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(mTabTitles[position]);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
