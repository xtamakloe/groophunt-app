package com.artoconnect.whatgroups.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.artoconnect.whatgroups.BR;
import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.binding.GroupListBindingHandlers;
import com.artoconnect.whatgroups.model.view.GroupVM;

import java.util.List;

/**
 * Created by Christian Tamakloe on 21/02/2016.
 *
 */
public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.GroupItemBindingHolder> {
    private List<GroupVM> mGroupVMs;

    public GroupListAdapter(List<GroupVM> groupVMs) {
        this.mGroupVMs = groupVMs; // NB: Uses view models
    }

    @Override
    public GroupItemBindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_group, parent, false);
        return new GroupItemBindingHolder(view);
    }

    @Override
    public void onBindViewHolder(GroupItemBindingHolder holder, int position) {
        final GroupVM groupVM = mGroupVMs.get(position);
        holder.getBinding().setVariable(BR.group, groupVM);
        holder.getBinding().setVariable(BR.handlers, new GroupListBindingHandlers(groupVM));
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mGroupVMs.size();
    }

    public static class GroupItemBindingHolder extends RecyclerView.ViewHolder {

        private ViewDataBinding mBinding;

        public GroupItemBindingHolder(View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
        }

        public ViewDataBinding getBinding() {
            return mBinding;
        }
    }
}
