package com.artoconnect.whatgroups.event;

/**
 * Created by Christian Tamakloe on 28/02/2016.
 */
public class RefreshGroupEvent {

    public final String mGroupJSON;
    public final int mAlertResId;

    public RefreshGroupEvent(String groupVMJSON, int alertResId) {
        this.mGroupJSON = groupVMJSON;
        this.mAlertResId = alertResId;
    }
}
