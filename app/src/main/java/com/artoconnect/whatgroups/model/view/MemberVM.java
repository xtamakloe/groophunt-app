package com.artoconnect.whatgroups.model.view;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.artoconnect.whatgroups.model.api.MemberAM;

/**
 * Created by Christian Tamakloe on 20/02/2016.
 */
public class MemberVM extends BaseObservable {
    private int id;
    private String username;
    private String phoneNo;

    /**
     * Convert {@link MemberAM} model to {@link MemberVM} model
     */
    public static MemberVM from(MemberAM memberAM) {
        MemberVM memberVM = new MemberVM();
        memberVM.setUsername(memberAM.getUsername());
        memberVM.setPhoneNo(memberAM.getPhoneNo());
        memberVM.setId(memberAM.getId());
//        memberVM.setAdminGroupIds(memberAM.getAdminGroupsIds());
//        memberVM.setJoinedGroupIds(memberAM.getJoinedGroupsIds());
        return memberVM;
    }

    @Bindable
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Bindable
    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Bindable
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /*
    private List<Integer> adminGroupIds = new ArrayList<>();
    private List<Integer> joinedGroupIds = new ArrayList<>();
    public List<Integer> getAdminGroupIds() {
        return adminGroupIds;
    }

    public void setAdminGroupIds(List<Integer> adminGroupIds) {
        this.adminGroupIds = adminGroupIds;
    }

    public List<Integer> getJoinedGroupIds() {
        return joinedGroupIds;
    }

    public void setJoinedGroupIds(List<Integer> joinedGroupIds) {
        this.joinedGroupIds = joinedGroupIds;
    }
    */
}
