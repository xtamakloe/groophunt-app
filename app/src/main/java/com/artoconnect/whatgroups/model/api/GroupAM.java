package com.artoconnect.whatgroups.model.api;

import java.util.List;

/**
 * Created by Christian Tamakloe on 25/02/2016.
 * <p/>
 * Group model as returned from API
 */
public class GroupAM {
    private int id;
    private String uid;
    private String name;
    private String description;
    private String pic_url;
    private List<MemberAM> members;
    private List<Integer> group_admins_ids;


    public List<MemberAM> getMembers() {
        return members;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPicUrl() {
        return pic_url;
    }

    public int getId() {
        return id;
    }

    public List<Integer> getAdminIds() {
        return group_admins_ids;
    }

    public String getUid() {
        return uid;
    }
}
