package com.artoconnect.whatgroups.model.api;

/**
 * Created by Christian Tamakloe on 25/02/2016.
 * <p/>
 * Member model based on JSON returned from API
 */
public class MemberAM {
    private int id;
    private String username;
    private String phone_no;


    public String getUsername() {
        return username;
    }

    public String getPhoneNo() {
        return phone_no;
    }

    public int getId() {
        return id;
    }

    /*private List<Integer> admin_groups_ids;
    private List<Integer> joined_groups_ids;

    public List<Integer> getAdminGroupsIds() {
        return admin_groups_ids;
    }

    public List<Integer> getJoinedGroupsIds() {
        return joined_groups_ids;
    }*/
}
