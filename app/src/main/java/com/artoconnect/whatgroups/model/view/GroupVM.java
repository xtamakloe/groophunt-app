package com.artoconnect.whatgroups.model.view;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.artoconnect.whatgroups.BR;
import com.artoconnect.whatgroups.model.api.GroupAM;
import com.artoconnect.whatgroups.model.api.MemberAM;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Christian Tamakloe on 20/02/2016.
 */
// TODO: Create separate model for api results that won't contain bindable annotations
public class GroupVM extends BaseObservable {
    private static final String TAG = "DBG_GroupVM";
    private int id;
    private String uid;
    private String name;
    private String description;
    private String picUrl;
    private List<MemberVM> memberVMs = new ArrayList<>(); // TODO: check init works
    private List<Integer> adminIds = new ArrayList<>();
    private int currentMemberId = -1; // Variable to hold current member, sometimes null

    /*
     * Convert {@link GroupAM} model to {@link GroupVM} model
     */
    public static GroupVM from(GroupAM groupAM) {
        GroupVM groupVM = new GroupVM();
        groupVM.setId(groupAM.getId());
        groupVM.setUid(groupAM.getUid());
        groupVM.setName(groupAM.getName());
        groupVM.setDescription(groupAM.getDescription());
        groupVM.setPicUrl(groupAM.getPicUrl());
        groupVM.setAdminIds(groupAM.getAdminIds());

        // Convert api member models to view models
        List<MemberVM> memberVMs = new ArrayList<MemberVM>();
        for (MemberAM memberAM : groupAM.getMembers()) {
            memberVMs.add(MemberVM.from(memberAM));
        }
        groupVM.setMemberVMs(memberVMs);

        return groupVM;
    }

    /**
     * Convert group of {@link GroupAM} models to {@link GroupVM} models
     */
    public static List<GroupVM> from(List<GroupAM> groupAMs) {
        List<GroupVM> groupVMs = new ArrayList<>();
        for (GroupAM groupAM : groupAMs) {
            groupVMs.add(from(groupAM));
        }
        return groupVMs;
    }

    @Bindable
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
        notifyPropertyChanged(BR.uid);
    }

    public int getCurrentMemberId() {
        return currentMemberId;
    }

    public void setCurrentMemberId(int currentMemberId) {
        this.currentMemberId = currentMemberId;
    }

    @Bindable
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    public List<MemberVM> getMemberVMs() {
        return memberVMs;
    }

    public void setMemberVMs(List<MemberVM> memberVMs) {
        this.memberVMs = memberVMs;
    }

    /**
     * Return list of members excluding admins
     *
     * @return
     */
    public List<MemberVM> getOnlyMemberVMs() {
        List<MemberVM> membersOnly = new ArrayList<>();
        for (MemberVM memberVM : getMemberVMs()) {
            if (!isAdmin(memberVM))
                membersOnly.add(memberVM);
        }
        return membersOnly;
    }

    public boolean isAdmin(MemberVM memberVM) {
        return getAdminIds().contains(memberVM.getId());
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }

    @Bindable
    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
        notifyPropertyChanged(BR.picUrl);
    }

    public String getRequestCount() {
        String number = "No";
        int onlyMembersCount = getOnlyMemberVMs().size();

        if (onlyMembersCount > 0)
            number = Integer.toString(onlyMembersCount);

        return number + " Requests";
    }

    public List<Integer> getMemberIds() {
        List<Integer> ids = new ArrayList<>();
        for (MemberVM memberVM : getMemberVMs()) {
            ids.add(memberVM.getId());
        }
        return ids;
    }

    public List<Integer> getAdminIds() {
        return adminIds;
    }

    public void setAdminIds(List<Integer> adminIds) {
        this.adminIds = adminIds;
    }

    public boolean currentMemberSentRequest() {
        return getMemberIds().contains(currentMemberId);
    }

    public boolean currentMemberIsAdmin() {
        return getAdminIds().contains(currentMemberId);
    }

    public String getSignUpPageIdUrl() {
        return "http://ghnt.it/s/" + Integer.toString(this.id);
    }

    public String getSignUpPagePinUrl(){
        return "http://ghnt.it/" + this.uid;
    }
}
