package com.artoconnect.whatgroups.model.db;

import com.artoconnect.whatgroups.model.view.MemberVM;

/**
 * Created by Christian Tamakloe on 25/02/2016.
 */
public class Member {
    private int id;
    private String username;
    private String phoneNo;

    /**
     * Convert member view model to db model
     *
     * @param memberVM
     * @return member
     */
    public static Member from(MemberVM memberVM) {
        Member member = new Member();
        member.setUsername(memberVM.getUsername());
        member.setPhoneNo(memberVM.getPhoneNo());
        member.setId(memberVM.getId());
        return member;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
}
