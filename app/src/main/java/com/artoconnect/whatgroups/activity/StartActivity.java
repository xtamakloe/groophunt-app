package com.artoconnect.whatgroups.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.api.APIResponse;
import com.artoconnect.whatgroups.api.APIService;
import com.artoconnect.whatgroups.fragment.LoginFragment;
import com.artoconnect.whatgroups.helper.SessionEventListener;
import com.artoconnect.whatgroups.helper.Settings;
import com.artoconnect.whatgroups.model.api.MemberAM;
import com.artoconnect.whatgroups.util.APIUtils;
import com.artoconnect.whatgroups.util.Utils;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StartActivity
        extends BaseActivity
        implements SessionEventListener {

    public static final String SIGNUP = "SIGN_UP";
    public static final String LOGIN = "LOG_IN";
    private static final String TAG = "DBG_StartActivity";
    private MaterialDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_start);

        Fragment fragment = LoginFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment, TAG)
                .commit();
    }


    @Override
    public void onProcessSessionRequest(String operation, String username, String phoneNo, String password) {
        if (validateInputs(username, phoneNo, password))
            makeAPICall(operation, username, phoneNo, password);
    }


    boolean validateInputs(String username, String phoneNo, String password) {
        boolean valid = true;
        int alert = 0;

        if (username != null) {
            if (username.isEmpty()) {
                valid = false;
                alert = R.string.session_error_invalid_username;
            }
        }

        if (phoneNo.isEmpty() || !phoneNo.startsWith("+")) {
            valid = false;
            alert = R.string.session_error_invalid_phone_no;
        }


        if (password.isEmpty() || password.length() < 6) {
            valid = false;
            alert = R.string.session_error_invalid_password;
        }

        if (alert != 0)
            showAlert(getResources().getString(alert));

        return valid;
    }


    void makeAPICall(String operation, String username, String phoneNo, String password) {

        if (!Utils.isConnected(getApplicationContext())) {
            Snackbar.make(findViewById(android.R.id.content),
                    R.string.network_error, Snackbar.LENGTH_LONG).show();
            return;
        }

        int title, content = -1;
        APIService.ApiInterface client = APIService.getClient();
        Call<APIResponse> call = null;

        if (operation.equals(SIGNUP)) {
            title = R.string.sign_up_dialog_title;
            content = R.string.sign_up_dialog_content;
            call = client.createMember(username, phoneNo, password);
        } else if (operation.equals(LOGIN)) {
            title = R.string.log_in_dialog_title;
            content = R.string.log_in_dialog_content;
            call = client.createSession(phoneNo, password);
        } else {
            // TODO: snack error
            return;
        }

        showProgressDialog(getResources().getString(title), getResources().getString(content));

        final Activity activity = this;

        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                hideProgressDialog();

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();

                    if (apiResponse.isSuccess()) {
                        MemberAM memberAM = data.getSession().getMember();
                        Settings.getInstance(getApplicationContext()).logIn(memberAM);

                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        finish();

                    } else if (apiResponse.isError()) {
                        showAlert(data.getError().getMessage());

                    } else {
                        Log.d(TAG, "apiResponse else");
                    }
                } else {
                    APIUtils.handleAPIResponseBodyNull(activity, response);
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                hideProgressDialog();

                APIUtils.handleRetrofitFailure(activity, t);
            }
        });
    }


    public void showProgressDialog(String title, String content) {
        mProgressDialog = new MaterialDialog.Builder(this)
                .title(title)
                .content(content)
                .progress(true, 0)
                .backgroundColor(ContextCompat.getColor(this, R.color.primary))
                .titleColor(ContextCompat.getColor(this, R.color.white))
                .contentColor(ContextCompat.getColor(this, R.color.white))
                .widgetColor(ContextCompat.getColor(this, R.color.white))
                .build();
        mProgressDialog.show();
    }


    public void hideProgressDialog() {
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
    }


    private void showAlert(String errorMsg) {
        Snackbar.make(findViewById(android.R.id.content),
                errorMsg,
                Snackbar.LENGTH_LONG).show();
    }
}
