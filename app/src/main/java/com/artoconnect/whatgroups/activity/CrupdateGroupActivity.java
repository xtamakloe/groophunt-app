package com.artoconnect.whatgroups.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.api.APIResponse;
import com.artoconnect.whatgroups.api.APIService;
import com.artoconnect.whatgroups.event.RefreshGroupEvent;
import com.artoconnect.whatgroups.event.RefreshGroupListEvent;
import com.artoconnect.whatgroups.helper.ImageDownloader;
import com.artoconnect.whatgroups.helper.Settings;
import com.artoconnect.whatgroups.model.api.GroupAM;
import com.artoconnect.whatgroups.model.view.GroupVM;
import com.artoconnect.whatgroups.util.APIUtils;
import com.artoconnect.whatgroups.util.ImageFilePath;
import com.artoconnect.whatgroups.util.Utils;
import com.google.gson.Gson;
import com.victor.loading.rotate.RotateLoading;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.artoconnect.whatgroups.util.RealPathUtil.getMimeType;

/**
 * This activity handles both creating and updating a group via API
 */
public class CrupdateGroupActivity extends BaseActivity {
    private static final String EDIT_OPERATION = "editOperation";
    private static final String NEW_OPERATION = "newOperation";
    private static final int PICK_IMAGE_REQUEST = 1;
    private static final String TAG = "DBG_NewGroupActivity";

    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.et_group_name)
    EditText mName;
    @Bind(R.id.et_group_desc)
    EditText mDesc;
    @Bind(R.id.tv_group_pic_label)
    TextView mPicLabel;
    @Bind(R.id.iv_group_pic)
    ImageView mPic;
    @Bind(R.id.sv_container_new_group)
    View mContainer;
    @Bind(R.id.pb_rotate)
    RotateLoading mProgressBar;

    private String mPicPath = null;
    private int mMemberId;
    private String mOperation;
    private GroupVM mGroupVM;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crupdate_group);
        ButterKnife.bind(this);

        mMemberId = Settings.getInstance(this).getCurrentMemberId();

        String groupJSON = getIntent().getStringExtra(GroupActivity.VM_JSON);
        mOperation = (groupJSON == null) ? NEW_OPERATION : EDIT_OPERATION;

        if (groupJSON != null) {
            mGroupVM = new Gson().fromJson(groupJSON, GroupVM.class);
            mOperation = EDIT_OPERATION;
        } else
            mOperation = NEW_OPERATION;

        initView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_crupdate_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_group_operation:
                saveGroup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.getData() != null) {

                    /*
                    Uri uri = data.getData();
                    Bitmap bitmap = MediaStore.Images.Media
                            .getBitmap(getContentResolver(), uri);
                    // Log.d(TAG, String.valueOf(bitmap));
                    mPic.setImageBitmap(bitmap);
                    mPicLabel.setVisibility(View.GONE);
                    mPicPath = RealPathUtil.getRealPathFromURI(this, data.getData());

                    // TODO: fix bug where only png images are picked and set by RealPathUtil...

                    Log.d(TAG, "realPath: "
                            + RealPathUtil.getRealPathFromURI(this, data.getData()));
                    */

                    Uri selectedImageUri = data.getData();

                    mPicLabel.setVisibility(View.GONE);
                    String selectedImagePath;
                    mPic.setImageURI(selectedImageUri);

                    //MEDIA GALLERY
                    selectedImagePath = ImageFilePath.getPath(getApplicationContext(), selectedImageUri);
//                    Log.i("Image File Path", "" + selectedImagePath);
                    mPicPath = selectedImagePath;

                }
            }
        }
    }

    void initView() {
        setSupportActionBar(mToolbar);
        Utils.setHomeAsUpIndicator(mToolbar, this);

        if (isEditOperation()) {
            mName.setText(mGroupVM.getName());
            mDesc.setText(mGroupVM.getDescription());

            String picUrl = mGroupVM.getPicUrl();
            if (!picUrl.equals("/images/placeholder.png"))
                new ImageDownloader(mPic).execute(picUrl);
        }
    }

    private void saveGroup() {
        // Check form inputs
        if (!hasValidInputs())
            return;

        int title = 0;
        int message = 0;
        if (isNewOperation()) {
            title = R.string.add_group_dialog_title;
            message = R.string.add_group_dialog_message;
        }
        if (isEditOperation()) {
            title = R.string.edit_group_dialog_title;
            message = R.string.edit_group_dialog_message;
        }

        // Show confirmation alert
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(title));
        builder.setMessage(getResources().getString(message));
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showLoader();

                makeAPICall();
            }
        });
        builder.setNegativeButton("NO", null);
        builder.show();
    }

    boolean isNewOperation() {
        return mOperation.equals(NEW_OPERATION);
    }

    boolean isEditOperation() {
        return mOperation.equals(EDIT_OPERATION);
    }

    private void makeAPICall() {

        if (!Utils.isConnected(this)) {
            Snackbar.make(findViewById(android.R.id.content),
                    R.string.network_error, Snackbar.LENGTH_LONG).show();
            return;
        }

        final String name = mName.getText().toString();
        final String description = mDesc.getText().toString();

        /*String imagePath = (mPicPath != null) ? mPicPath : getDefaultImagePath(this);
        // "/data/data/com.artoconnect.whatgroups/files/placeholder_256.png"
        // Log.d("DBG_imagePath", imagePath);

        RequestBody rqbFile = RequestBody.create(
                MediaType.parse(getMimeType(imagePath)), new File(imagePath));*/

        if (!hasValidInputs())
            return;

        Log.d(TAG, "mPicPath: " + mPicPath);
        RequestBody rqbFile = null;
        if (mPicPath != null) {
            String mimeType = getMimeType(mPicPath);
            rqbFile = RequestBody.create(
                    MediaType.parse("image/png"), new File(mPicPath));
        }

        RequestBody rqbDesc = RequestBody.create(
                MediaType.parse("multipart/form-data"), "Group Picture");

        APIService.ApiInterface client = APIService.getClient();
        Call<APIResponse> call = null;

        if (isEditOperation())
            call = client.updateGroup(mGroupVM.getId(), mMemberId, name, description, rqbFile, rqbDesc);

        else if (isNewOperation())
            call = client.createGroup(mMemberId, name, description, rqbFile, rqbDesc);

        if (call != null) {
            final Activity activity = this;

            call.enqueue(new Callback<APIResponse>() {
                @Override
                public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                    hideLoader();

                    if (response.body() != null) {
                        APIResponse apiResponse = response.body();
                        APIResponse.Data data = apiResponse.getData();

                        if (apiResponse.isSuccess()) {

                            // Get group and convert to view model then to json for transport
                            GroupAM groupAM = data.getGroup();
                            String groupVMJSON = new Gson().toJson(GroupVM.from(groupAM));

                            // After create operation, open new activity to show group
                            if (isNewOperation()) {
                                Intent intent = new Intent(getApplicationContext(), GroupActivity.class);
                                // Convert group api model returned to view model and send
                                intent.putExtra(GroupActivity.VM_JSON, groupVMJSON);
                                intent.putExtra(GroupActivity.ALERT,
                                        groupAM.getName() + " added successfully");
                                startActivity(intent);

                            }
                            // After update operation, return to previous activity and refresh it
                            else if (isEditOperation()) {
                                // Refresh previous window => which is the group window
                                // should be a start activity for result
                                // so detach and reattach fragment to update it
//                                or i could just use eventbus
                                int alertResId = R.string.group_updated;
                                EventBus.getDefault().post(
                                        new RefreshGroupEvent(groupVMJSON, alertResId));
                            }

                            finish();

                            // In both cases, refresh the home list of groups
                            EventBus.getDefault().post(new RefreshGroupListEvent());

                        } else if (apiResponse.isError()) {
                            showAlert(data.getError().getMessage());

                        } else {
                            Log.d(TAG, "Retrofit Else");
                        }
                    } else { // response.body() == null
                        APIUtils.handleAPIResponseBodyNull(activity, response);
                    }
                }

                @Override
                public void onFailure(Call<APIResponse> call, Throwable t) {
                    hideLoader();

                    APIUtils.handleRetrofitFailure(activity, t);
                }
            });
        }


    }

    private void showLoader() {
        mContainer.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.start();
    }

    private void hideLoader() {
        mContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mProgressBar.stop();
    }

    public void startActivityToPickImage(View view) {
        Intent intent = new Intent();
        // Show only images, no videos or anything else
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        // Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    private boolean hasValidInputs() {
        boolean valid = true;
        // Group name is mandatory
        if (mName.getText().toString().isEmpty()) {
            mName.setError("Please enter a name for the group");
            valid = false;
        } else
            mName.setError(null);
        return valid;
    }

    private void showAlert(String errorMsg) {
        Snackbar.make(findViewById(android.R.id.content),
                errorMsg,
                Snackbar.LENGTH_LONG).show();
    }
}
