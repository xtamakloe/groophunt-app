package com.artoconnect.whatgroups.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.adapter.HomeScreenPagerAdapter;
import com.artoconnect.whatgroups.helper.Settings;
import com.artoconnect.whatgroups.util.RateThisApp;
import com.artoconnect.whatgroups.util.Utils;

import butterknife.Bind;
import butterknife.ButterKnife;


public class HomeActivity extends BaseActivity {

    private static final String TAG = "DBG_HomeActivity";
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.vp_group_pages)
    ViewPager mViewPager;
    @Bind(R.id.tl_tab_titles)
    TabLayout mTabLayout;

    private HomeScreenPagerAdapter mAdapter;
    private MenuItem mSearchItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initView();

        login();
    }


    void initView() {
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        mViewPager.setOffscreenPageLimit(2);
        mAdapter = new HomeScreenPagerAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }


    private void login() {
        if (Settings.getInstance(this).getCurrentMemberId() == -1) {
            authenticateLogin();
            finish();
        }
    }

    private void authenticateLogin() {
        startActivity(new Intent(this, StartActivity.class));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        mSearchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(mSearchItem);

        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
//        searchView.setIconifiedByDefault(false);
//        searchView.setSubmitButtonEnabled(true);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // add group
            case R.id.action_new_group:
                startActivity(new Intent(this, CrupdateGroupActivity.class));
                return true;

            // share app
            case R.id.action_share_app:
                Utils.share(this, getResources().getString(R.string.share_app_message));
                return true;

            // Rate app
            case R.id.action_rate_app:
                RateThisApp.showRateDialog(this);
                return true;

            // Contact Support
            case R.id.action_support:
                Intent Email = new Intent(Intent.ACTION_SEND);
                Email.setType("message/rfc822");
                Email.putExtra(Intent.EXTRA_EMAIL, new String[]{"apps@groophunt.com"});
                Email.putExtra(Intent.EXTRA_SUBJECT,
                        "GroopHunt Support - M" +
                                Integer.toString(Settings.getInstance(this).getCurrentMemberId()));
                Email.putExtra(Intent.EXTRA_TEXT, "Hullo GroopHunt Team, ");
                startActivity(Intent.createChooser(Email, "We'd love to hear from you!"));
                return true;

            // logout
            case R.id.action_logout:
                Settings.getInstance(this).logOut();
                startActivity(new Intent(this, StartActivity.class));
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onFabClick(View view) {
        MenuItemCompat.expandActionView(mSearchItem);
    }
}
