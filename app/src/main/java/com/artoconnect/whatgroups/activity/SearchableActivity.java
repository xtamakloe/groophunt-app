package com.artoconnect.whatgroups.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.adapter.GroupListAdapter;
import com.artoconnect.whatgroups.api.APIResponse;
import com.artoconnect.whatgroups.api.APIService;
import com.artoconnect.whatgroups.databinding.ActivitySearchableBinding;
import com.artoconnect.whatgroups.model.view.GroupVM;
import com.artoconnect.whatgroups.util.APIUtils;
import com.artoconnect.whatgroups.util.Utils;
import com.victor.loading.rotate.RotateLoading;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchableActivity
        extends BaseActivity
        implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private static final String TAG = "DBG_SearchableActivity";
    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Bind(R.id.swipeContainer)
    SwipeRefreshLayout mSwipeContainer;
    @Bind(R.id.rv_group_list)
    RecyclerView mRecyclerView;
    @Bind(R.id.pb_rotate)
    RotateLoading mLoader;
    @Bind(R.id.btn_retry)
    Button mRetryButton;

    private GroupListAdapter mAdapter;
    private ActivitySearchableBinding mBinding;
    private String mQuery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_searchable);

        initView();

        handleIntent(getIntent());
    }


    void initView() {
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        Utils.setHomeAsUpIndicator(mToolbar, this);

        mBinding.setEmptyText(getResources().getString(R.string.empty_text_search));

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mSwipeContainer.setColorSchemeResources(R.color.primary);
        mSwipeContainer.setOnRefreshListener(this);

        mRetryButton.setOnClickListener(this);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }


    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            mQuery = intent.getStringExtra(SearchManager.QUERY);
            getSupportActionBar().setTitle("Results for " + mQuery);

            loadGroups();
        }
    }


    private void loadGroups() {
        hideRetryButton();

        makeAPICall();
    }

    private void makeAPICall() {
        if (!Utils.isConnected(this)) {
            Snackbar.make(this.findViewById(android.R.id.content),
                    R.string.network_error, Snackbar.LENGTH_LONG).show();
            return;
        }

        showLoader();

        final Activity activity = this;
        Call<APIResponse> call = APIService.getClient().getGroupsByUidOrName(mQuery);
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                hideLoader();

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();

                    if (apiResponse.isSuccess()) {
                        mAdapter = new GroupListAdapter(GroupVM.from(data.getGroups())); // Convert to VM first
                        mRecyclerView.setAdapter(mAdapter);
                        mBinding.setDatasetSize(data.getGroups().size());

                    } else if (apiResponse.isError()) {
                        Log.d("DBG_getGroups", "error");

                    } else {
                        Log.d("DBG_getGroups", "not error not success");
                    }
                } else {
                    APIUtils.handleAPIResponseBodyNull(activity, response);
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                hideLoader();
                showRetryButton();
                Log.d("DBG_getGroups", "ERroR:" + t.toString());
            }
        });
    }


    void showLoader() {
        mLoader.start();
        mBinding.setOperationRunning(true);
    }

    void hideLoader() {
        mLoader.stop();
        mBinding.setOperationRunning(false);
    }

    void showRetryButton() {
        mBinding.setShowRetry(true);
    }

    void hideRetryButton() {
        mBinding.setShowRetry(false);
    }

    @Override
    public void onRefresh() {
        mSwipeContainer.setRefreshing(false);
        loadGroups();
    }

    @Override
    public void onClick(View v) {
        loadGroups();
    }
}
