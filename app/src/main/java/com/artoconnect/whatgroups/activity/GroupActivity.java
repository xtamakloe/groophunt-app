package com.artoconnect.whatgroups.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.fragment.GroupFragment;

public class GroupActivity extends BaseActivity {

    public static final String VM_JSON = "groupVMJSON";
    public static final String ALERT = "groupAlert";
    private static final String TAG = "DBG_GroupActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        Fragment fragment = GroupFragment.newInstance(
                getIntent().getStringExtra(VM_JSON), getIntent().getStringExtra(ALERT));
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, fragment, GroupFragment.TAG)
                .commit();
    }
}
