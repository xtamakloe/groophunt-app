package com.artoconnect.whatgroups.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

import com.artoconnect.whatgroups.R;

import java.util.Locale;

/**
 * Created by Christian Tamakloe on 20/02/2016.
 */
public class Utils {
    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static String titleize(String src){
        String source = src.toLowerCase();
        boolean cap = true;
        char[]  out = source.toCharArray();
        int i, len = source.length();
        for(i=0; i<len; i++){
            if(Character.isWhitespace(out[i])){
                cap = true;
                continue;
            }
            if(cap){
                out[i] = Character.toUpperCase(out[i]);
                cap = false;
            }
        }
        return new String(out);
    }

    public static void setHomeAsUpIndicator(Toolbar toolbar, final Activity activity) {
        // Show back arrow and handle navigation (works for LG bugs)
        toolbar.setNavigationIcon(
                ContextCompat.getDrawable(activity, R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onBackPressed();
            }
        });
    }

    public static void share(Context context, String message) {
//        https://play.google.com/store/apps/details?id=com.artoconnect.whatgroups
        Intent shareIntent = new Intent(Intent.ACTION_SEND).setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        context.startActivity(Intent.createChooser(shareIntent,
                context.getResources().getString(R.string.share_via)));
    }




    public static String getDefaultCountryCode(Context context) {
        String countryCode = "";
        try {
            TelephonyManager tm =
                    (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            countryCode = tm.getNetworkCountryIso();
        } catch (Exception e) {
            countryCode = Locale.getDefault().getCountry();
        }
        return countryCode;
    }
}
