package com.artoconnect.whatgroups.util;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.artoconnect.whatgroups.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Christian Tamakloe on 21/02/2016.
 */
public class BindingUtils {
    @BindingAdapter({"bind:imageUrl", "bind:placeholder"})
    public static void loadImage(ImageView view, String url, Drawable placeholder) {
        Picasso.with(view.getContext())
                .load(url)
                .placeholder(placeholder)
                .error(placeholder)
                .noFade()
                .into(view);
    }
}