package com.artoconnect.whatgroups.util;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.api.APIResponse;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.Response;


/**
 * Created by Christian Tamakloe on 04/03/2016.
 */
public class APIUtils {
    public static void handleAPIResponseBodyNull(Activity activity, Response response) {
        try {
            String errorJson = response.errorBody().string();
            APIResponse errorResponse =
                    new Gson().fromJson(errorJson, APIResponse.class);
            showAlert(activity, errorResponse.getData().getError().getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void handleRetrofitFailure(Activity activity, Throwable t) {
        String exceptionMsg = null;
        try {
            throw t;
        } catch (SocketTimeoutException e) {
            exceptionMsg = activity.getResources().getString(R.string.socket_timeout_error);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            exceptionMsg = activity.getResources().getString(R.string.general_connection_error);
        } finally {
            showAlert(activity, exceptionMsg);
            Log.d("APIUtils", "Retrofit onFailure: " + t.toString());
        }
    }


    private static void showAlert(Activity activity, String errorMsg) {
        Snackbar.make(activity.findViewById(android.R.id.content),
                errorMsg,
                Snackbar.LENGTH_LONG).show();
    }
}
