package com.artoconnect.whatgroups.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.adapter.GroupListAdapter;
import com.artoconnect.whatgroups.api.APIResponse;
import com.artoconnect.whatgroups.api.APIService;
import com.artoconnect.whatgroups.databinding.FragmentGroupListBinding;
import com.artoconnect.whatgroups.event.RefreshGroupListEvent;
import com.artoconnect.whatgroups.helper.Settings;
import com.artoconnect.whatgroups.model.view.GroupVM;
import com.artoconnect.whatgroups.util.APIUtils;
import com.artoconnect.whatgroups.util.Utils;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupListFragment extends Fragment {

    public static final int ALL = -1;
    public static final int REQUESTS = -2;
    public static final int ADDED = -3;
    //    public static final int SEARCH = -4;
    private static final String ARG_LIST_TYPE = "LIST_TYPE";
    //    private static final String ARG_QUERY = "QUERY.";
    private static final String TAG = "DBG_GroupListFragment";

    @Bind(R.id.swipeContainer)
    SwipeRefreshLayout mSwipeContainer;
    @Bind(R.id.rv_group_list)
    RecyclerView mRecyclerView;
    @Bind(R.id.pb_rotate)
    RotateLoading mLoader;
    @Bind(R.id.btn_retry)
    Button mRetryButton;

    private GroupListAdapter mAdapter;
    private FragmentGroupListBinding mBinding;
    private int mListType;
    private int mCurrentMemberId;
//    private String mQuery;


    public GroupListFragment() {
        // Required empty public constructor
    }


    public static GroupListFragment newInstance(int listType) {
        GroupListFragment fragment = new GroupListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_LIST_TYPE, listType);
//        args.putString(ARG_QUERY, query);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_group_list, container, false);
        View view = mBinding.getRoot();
        ButterKnife.bind(this, view);

        EventBus.getDefault().register(this);
        Picasso.with(getContext()).setIndicatorsEnabled(true);

        mCurrentMemberId = Settings.getInstance(getContext()).getCurrentMemberId();
        mListType = getArguments().getInt(ARG_LIST_TYPE, ALL);
//        mQuery = getArguments().getString(ARG_QUERY);

        initView(view);
        return view;
    }


    private void initView(View rootView) {
        rootView.setTag(TAG);

        mBinding.setEmptyText(getEmptyText());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mSwipeContainer.setColorSchemeResources(R.color.primary);
//        mSwipeContainer.setColorSchemeColors(0,0,0,0);
//        mSwipeContainer.setProgressBackgroundColorSchemeResource(android.R.color.transparent);
        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeContainer.setRefreshing(false);
                loadGroups();
            }
        });

        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadGroups();
            }
        });

        loadGroups();
    }


    private void loadGroups() {
        hideRetryButton();

        makeAPICall();
    }


    private void makeAPICall() {
        if (!Utils.isConnected(getContext())) {
            Snackbar.make(getActivity().findViewById(android.R.id.content),
                    R.string.network_error, Snackbar.LENGTH_LONG).show();
            showRetryButton();
            return;
        }

        showLoader();

        mCurrentMemberId = Settings.getInstance(getContext()).getCurrentMemberId();

        APIService.ApiInterface client = APIService.getClient();
        Call<APIResponse> call;

        // Get signed-up/joined groups
        if (isRequestsList()) {
            call = client.getGroupsByMemberRequest(mCurrentMemberId);
        }
        // Get added list
        else if (isAddedList()) {
            call = client.getGroupsByMemberAdmin(mCurrentMemberId);
        }
        // Get search results
//        else if (isSearchList()) {
//            call = client.getGroupsByUidOrName(mQuery);
//        }
        // Get all groups
        else
            call = client.getGroups();

        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                hideLoader();

                if (response.body() != null) {
                    APIResponse apiResponse = response.body();
                    APIResponse.Data data = apiResponse.getData();

                    if (apiResponse.isSuccess()) {
                        mAdapter = new GroupListAdapter(GroupVM.from(data.getGroups())); // Convert to VM first
                        mRecyclerView.setAdapter(mAdapter);
                        mBinding.setDatasetSize(data.getGroups().size());

                    } else if (apiResponse.isError()) {
                        Log.d("DBG_getGroups", "error");

                    } else {
                        Log.d("DBG_getGroups", "not error not success");
                    }
                } else {
                    APIUtils.handleAPIResponseBodyNull(getActivity(), response);
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                hideLoader();
                showRetryButton();
                Log.d("DBG_getGroups", "ERroR:" + t.toString());
            }
        });
    }

    private String getEmptyText() {
        int resId;
        if (isRequestsList()) {
            resId = R.string.empty_text_signups;
        } else if (isAddedList()) {
            resId = R.string.empty_text_added;
        } else {
            resId = R.string.empty_text_all;
        }
        return getResources().getString(resId);
    }

    void showLoader() {
        mLoader.start();
        mBinding.setOperationRunning(true);
    }

    void hideLoader() {
        mLoader.stop();
        mBinding.setOperationRunning(false);
    }

    void showRetryButton() {
        mBinding.setShowRetry(true);
    }

    void hideRetryButton() {
        mBinding.setShowRetry(false);
    }

    @Subscribe
    public void onEvent(RefreshGroupListEvent event) {
        loadGroups();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    boolean isAllGroupsList() {
        return mListType == ALL;
    }

    boolean isRequestsList() {
        return mListType == REQUESTS;
    }

    boolean isAddedList() {
        return mListType == ADDED;
    }

//    boolean isSearchList() {
//        return mListType == SEARCH;
//    }
}
