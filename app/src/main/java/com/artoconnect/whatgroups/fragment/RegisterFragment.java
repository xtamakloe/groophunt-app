package com.artoconnect.whatgroups.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.activity.StartActivity;
import com.artoconnect.whatgroups.helper.SessionEventListener;
import com.artoconnect.whatgroups.util.Utils;

import net.rimoto.intlphoneinput.IntlPhoneInput;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    private static final String TAG = "DBG_RegisterFragment";


    @Bind(R.id.et_username)
    EditText mUsernameField;
    @Bind(R.id.et_phone_no)
    IntlPhoneInput mPhoneNoField;
    @Bind(R.id.et_password)
    EditText mPasswordField;


    public RegisterFragment() {
        // Required empty public constructor
    }


    public static Fragment newInstance() {
        return new RegisterFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);

        mPhoneNoField.setEmptyDefault(Utils.getDefaultCountryCode(getContext()));

        return view;
    }


    @OnClick(R.id.rl_login_link)
    void switchToLoginScreen() {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, LoginFragment.newInstance(), TAG)
                .commit();
    }


    @OnClick(R.id.btn_register)
    void register() {
        String username = mUsernameField.getText().toString().trim();
        String password = mPasswordField.getText().toString().trim();
        String phoneNo = "";

        if (mPhoneNoField.isValid())
            phoneNo = mPhoneNoField.getNumber();

        ((SessionEventListener) getActivity())
                .onProcessSessionRequest(StartActivity.SIGNUP, username, phoneNo, password);
    }
}
