package com.artoconnect.whatgroups.fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.activity.CrupdateGroupActivity;
import com.artoconnect.whatgroups.activity.GroupActivity;
import com.artoconnect.whatgroups.api.APIResponse;
import com.artoconnect.whatgroups.api.APIService;
import com.artoconnect.whatgroups.binding.GroupBindingHandlers;
import com.artoconnect.whatgroups.databinding.FragmentGroupBinding;
import com.artoconnect.whatgroups.event.RefreshGroupEvent;
import com.artoconnect.whatgroups.event.RefreshGroupListEvent;
import com.artoconnect.whatgroups.helper.ContactHelper;
import com.artoconnect.whatgroups.helper.Settings;
import com.artoconnect.whatgroups.model.view.GroupVM;
import com.artoconnect.whatgroups.util.APIUtils;
import com.artoconnect.whatgroups.util.Utils;
import com.google.gson.Gson;
import com.victor.loading.rotate.RotateLoading;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment
        extends Fragment
        implements GroupBindingHandlers.GroupClickListener {

    public static final String TAG = "DBG_GroupFragment";
    private static final String SEND_REQUEST = "SEND_REQUEST";
    private static final String CANCEL_REQUEST = "CANCEL_REQUEST";
    private static final String DELETE_REQUEST = "DELETE_GROUP";
    private static final String ARG_GROUP_JSON = "arg.GroupJson";
    private static final String ARG_ALERT = "arg.Alert";

    @Bind(R.id.cl_root_view)
    CoordinatorLayout mRootView;
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.cv_group_details)
    View mContainer;
    @Bind(R.id.pb_rotate)
    RotateLoading mProgressBar;

    //    private CoordinatorLayout mRootView;
    private int mMemberId;
    //    private RotateLoading mProgressBar;
//    private View mContainer;
    private APIService.ApiInterface mClient;
    private GroupVM mGroupVM;
    private FragmentGroupBinding mBinding;
    private GroupBindingHandlers mHandler;

    public GroupFragment() {
        // Required empty public constructor
    }

    public static GroupFragment newInstance(String groupJSON, String alert) {
        GroupFragment fragment = new GroupFragment();
        Bundle args = new Bundle();
        args.putString(ARG_GROUP_JSON, groupJSON);
        args.putString(ARG_ALERT, alert);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_group, container, false);
        View view = mBinding.getRoot();
        ButterKnife.bind(this, view);
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        initView();
        return view;
    }

    private void initView() {
//        mGroupVM = new Gson().fromJson(getArguments().getString(ARG_GROUP_JSON), GroupVM.class);
        mGroupVM = new Gson().fromJson(getArguments().getString(ARG_GROUP_JSON), GroupVM.class);
        mMemberId = Settings.getInstance(getActivity()).getCurrentMemberId();
        mGroupVM.setCurrentMemberId(mMemberId);

        mClient = APIService.getClient();
        mHandler = new GroupBindingHandlers(mGroupVM, this);

        mBinding.setGroup(mGroupVM);
        mBinding.setHandlers(mHandler);

        // Complete view setup
        mToolbar.setTitle(mGroupVM.getName());
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        // Show back arrow and handle navigation (works for LG bugs)
        Utils.setHomeAsUpIndicator(mToolbar, getActivity());

        setHasOptionsMenu(true);

        String alert = getArguments().getString(ARG_ALERT);
        if (alert != null)
            showAlert(alert);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (mGroupVM.currentMemberIsAdmin())
            inflater.inflate(R.menu.menu_group_admin, menu);
        else
            inflater.inflate(R.menu.menu_group_member, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete_group:
                showConfirmationDialog(
                        DELETE_REQUEST, R.string.delete_group_title, R.string.delete_group_msg);
                break;

            case R.id.action_edit_group:
                launchEditor();
                break;

            case R.id.action_share_group:
//                String linkAppMsg = " Request an invite via GroopHunt" +
//                        " (http://bit.ly/1p7lWdJ; Search for Group PIN: " +
                String linkAppMsg = " Request to join via GroopHunt "
                        + mGroupVM.getSignUpPagePinUrl();
                String message = mGroupVM.getDescription() + linkAppMsg;

                Utils.share(getActivity(), message);
                break;

            case R.id.action_save_requests:
                saveSignups();
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveSignups() {
        ContactHelper.saveToContacts(getActivity().getContentResolver(), getContext(), mGroupVM);
        showAlert(getResources().getString(R.string.signups_saved));
    }

    private void launchEditor() {
        Intent intent = new Intent(getContext(), CrupdateGroupActivity.class);
        // serialize group object to json and put into intent
        intent.putExtra(GroupActivity.VM_JSON, new Gson().toJson(mGroupVM));
        getContext().startActivity(intent);
    }

    @Override
    public void onClickSendRequest() {
        showConfirmationDialog(SEND_REQUEST,
                R.string.send_request_title, R.string.send_request_msg);
    }

    @Override
    public void onClickCancelRequest() {
        showConfirmationDialog(CANCEL_REQUEST,
                R.string.cancel_request_title, R.string.cancel_request_msg);
    }

    void showConfirmationDialog(final String operation, int titleResId, int bodyResId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(titleResId));
        builder.setMessage(getResources().getString(bodyResId));

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showLoader();

                makeAPICall(operation);
            }
        });
        builder.setNegativeButton("NO", null);
        builder.show();
    }

    private void showLoader() {
        mContainer.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.start();
    }

    private void hideLoader() {
        mContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mProgressBar.stop();
    }

    private void showAlert(String alert) {
        if (alert != null)
            Snackbar.make(mRootView, alert, Snackbar.LENGTH_LONG).show();
    }

    private void makeAPICall(final String requestType) {
        if (!Utils.isConnected(getContext())) {
            Snackbar.make(getActivity().findViewById(android.R.id.content),
                    R.string.network_error, Snackbar.LENGTH_LONG).show();
            return;
        }

        Call<APIResponse> request = null;
        if (requestType.equals(SEND_REQUEST))
            request = mClient.requestMembership(mGroupVM.getId(), mMemberId);

        if (requestType.equals(CANCEL_REQUEST))
            request = mClient.cancelMembership(mGroupVM.getId(), mMemberId);

        if (requestType.equals(DELETE_REQUEST))
            request = mClient.deleteGroup(mGroupVM.getId(), mMemberId);

        assert request != null;

        request.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                handleResponse(requestType, response);
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                handleError(t);
            }
        });
    }

    void handleResponse(String operation, Response<APIResponse> response) {
        hideLoader();

        if (response.body() != null) {
            APIResponse apiResponse = response.body();
            APIResponse.Data data = apiResponse.getData();

            if (apiResponse.isSuccess()) {
                // refresh requests list on grouplist
                EventBus.getDefault().post(new RefreshGroupListEvent());

                // refresh activity with success messsage and updated group details
                int alert;
                if (operation.equals(SEND_REQUEST) || operation.equals(CANCEL_REQUEST)) {
                    alert = operation.equals(SEND_REQUEST) ? R.string.request_sent : R.string.request_cancelled;

                    String groupJSON = new Gson().toJson(GroupVM.from(data.getGroup()));
                    refreshView(groupJSON, alert);
                }

                if (operation.equals(DELETE_REQUEST)) {
                    alert = R.string.group_deleted;
                    getActivity().finish();
                }

            } else if (apiResponse.isError()) {
                showAlert(data.getError().getMessage());

            } else {
                Log.d(TAG, "Retrofit Else");
            }
        } else { // response.body() == null
            APIUtils.handleAPIResponseBodyNull(getActivity(), response);
        }
    }

    private void refreshView(String groupJSON, int alertResId) {
        Fragment frg = null;
        frg = getFragmentManager().findFragmentByTag(TAG);
        // Update fragment args with updated group info
        frg.getArguments().putString(ARG_GROUP_JSON, groupJSON);
        frg.getArguments().putString(ARG_ALERT, getResources().getString(alertResId));
        getFragmentManager()
                .beginTransaction()
                .detach(frg)
                .attach(frg)
//                .commit()
                .commitAllowingStateLoss();
    }

    void handleError(Throwable t) {
        hideLoader();

        APIUtils.handleRetrofitFailure(getActivity(), t);
    }

    @Subscribe
    public void onEvent(RefreshGroupEvent event) {
        refreshView(event.mGroupJSON, event.mAlertResId);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
