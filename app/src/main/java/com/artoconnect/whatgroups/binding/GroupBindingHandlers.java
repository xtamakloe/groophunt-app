package com.artoconnect.whatgroups.binding;

import android.view.View;

import com.artoconnect.whatgroups.model.view.GroupVM;

/**
 * Created by Christian Tamakloe on 26/02/2016.
 */
public class GroupBindingHandlers {
    private final GroupVM mGroupVM;
    private final GroupClickListener mCallback;

    public GroupBindingHandlers(GroupVM mGroupVM, GroupClickListener callback) {
        this.mGroupVM = mGroupVM;
        this.mCallback = callback;
    }

    public void onClickSendRequest(View view) {
        mCallback.onClickSendRequest();
    }

    public void onClickCancelRequest(View view) {
        mCallback.onClickCancelRequest();
    }

    public interface GroupClickListener {
        void onClickCancelRequest();

        void onClickSendRequest();
    }
}
