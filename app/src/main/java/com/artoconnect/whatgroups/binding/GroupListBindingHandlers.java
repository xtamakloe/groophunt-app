package com.artoconnect.whatgroups.binding;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.artoconnect.whatgroups.activity.GroupActivity;
import com.artoconnect.whatgroups.model.view.GroupVM;
import com.google.gson.Gson;

/**
 * Created by Christian Tamakloe on 24/02/2016.
 */
public class GroupListBindingHandlers {
    private final GroupVM mGroupVM;

    public GroupListBindingHandlers(GroupVM groupVM) {
        this.mGroupVM = groupVM;
    }

    public void onClickGroup(View view) {
        Context context = view.getContext();
        Intent intent = new Intent(context, GroupActivity.class);
        // serialize group object to json and put into intent
        intent.putExtra(GroupActivity.VM_JSON, new Gson().toJson(mGroupVM));
        context.startActivity(intent);
    }
}
