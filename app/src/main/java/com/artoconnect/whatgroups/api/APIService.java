package com.artoconnect.whatgroups.api;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by Christian Tamakloe on 20/02/2016.
 */
public class APIService {
//    public static final String API_BASE_URL = "http://10.0.3.2:3000/api/v1/";
    public static final String API_BASE_URL = "http://linkappp.herokuapp.com/api/v1/";
    private static ApiInterface sApiInterface;

    public static ApiInterface getClient() {
        if (sApiInterface == null) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
//                    .client(getOkHttpClient()) // Inject client for debugging
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            sApiInterface = retrofit.create(ApiInterface.class);
        }
        return sApiInterface;
    }

    public static OkHttpClient getOkHttpClient() {
        // http://inthecheesefactory.com/blog/retrofit-2.0/en
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());

                String content = response.body().string();

                Log.d("DBG message: ", response.message());
                Log.d("DBG code: ", Integer.toString(response.code()));
                Log.d("DBG content:", content);

                // {"status"=>"error", "data"=>{"error"=>{"message"=>"Internal server error"}}}
                // {"status"=>"error", "data"=>{"error"=>{"message"=>"Member not found"}}}

                // Gson gson = new Gson();
                // APIResponse apiResponse = gson.fromJson(content, APIResponse.class);

                // Log.d("DBG", apiResponse.getData().getError().toString());
                // Log.d("DBG", Integer.toString(apiResponse.getData().getError().getCode()));

                // Return original
                return response.newBuilder()
                        .body(ResponseBody.create(response.body().contentType(), content))
                        .build();
            }
        }).build();
        return client.build();
    }

    /**
     * API Interface
     */
    public interface ApiInterface {

        // All groups
        @GET("groups")
        Call<APIResponse> getGroups();

        // Search groups
        @GET("groups")
        Call<APIResponse> getGroupsByUidOrName(
                @Query("query") String query
        );

         // Signups
        @GET("groups?admin=0")
        Call<APIResponse> getGroupsByMemberRequest(
                @Query("member_id") int memberId
        );

        // Added groups
        @GET("groups?admin=1")
        Call<APIResponse> getGroupsByMemberAdmin(
                @Query("member_id") int memberId
        );

        @Multipart
        @POST("groups")
        Call<APIResponse> createGroup(
                @Query("member_id") int memberId,
                @Query("group_name") String name,
                @Query("group_description") String groupDescription,
                @Part("pic\"; filename=\"image.png\" ") RequestBody file,
                @Part("description") RequestBody imageDescription
        );

        @Multipart
        @PUT("groups/{id}")
        Call<APIResponse> updateGroup(
                @Path("id") int groupId,
                @Query("member_id") int memberId,
                @Query("group_name") String name,
                @Query("group_description") String groupDescription,
                @Part("pic\"; filename=\"image.png\" ") RequestBody file,
                @Part("description") RequestBody imageDescription
        );

        @GET("groups/{id}")
        Call<APIResponse> getGroupInfo(
                @Path("id") int groupId
        );

        @DELETE("groups/{id}")
        Call<APIResponse> deleteGroup(
                @Path("id") int groupId,
                @Query("member_id") int memberId
        );

        @POST("memberships")
        Call<APIResponse> requestMembership(
                @Query("group_id") int groupId,
                @Query("member_id") int memberId
        );

        @DELETE("memberships")
        Call<APIResponse> cancelMembership(
                @Query("group_id") int groupId,
                @Query("member_id") int memberId
        );

        // should return memberId
        @POST("sessions")
        Call<APIResponse> createSession(
                @Query("phone_no") String phoneNo,
                @Query("password") String password
            );

        // should return memberId
        @POST("members")
        Call<APIResponse> createMember(
                @Query("username") String username,
                @Query("phone_no") String phoneNo,
                @Query("password") String password
        );



    }
}
