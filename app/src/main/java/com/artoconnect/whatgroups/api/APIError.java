package com.artoconnect.whatgroups.api;

/**
 * Created by Christian Tamakloe on 21/02/2016.
 */
public class APIError {
    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}