package com.artoconnect.whatgroups.api;

import com.artoconnect.whatgroups.model.api.GroupAM;
import com.artoconnect.whatgroups.model.api.MemberAM;
import com.artoconnect.whatgroups.model.api.SessionAM;

import java.util.List;

/**
 * Created by Christian Tamakloe on 21/02/2016.
 */
public class APIResponse {
    private String status;
    private Data data;

    public boolean isSuccess() {
        return status != null && status.equals("success");
    }

    public boolean isFail() {
        return status != null && status.equals("fail");
    }

    public boolean isError() {
        return status != null && status.equals("error");
    }

    public Data getData() {
        return data;
    }

    public class Data {
        APIError error;
        private List<GroupAM> groups;
        private GroupAM group;
        private MemberAM member;
        private SessionAM session;

        public SessionAM getSession() {
            return session;
        }

        public GroupAM getGroup() {
            return group;
        }

        public APIError getError() {
            return error;
        }

        public List<GroupAM> getGroups() {
            return groups;
        }
    }
}
