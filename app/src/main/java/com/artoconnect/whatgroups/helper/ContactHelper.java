package com.artoconnect.whatgroups.helper;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.util.Log;

import com.artoconnect.whatgroups.R;
import com.artoconnect.whatgroups.model.view.GroupVM;
import com.artoconnect.whatgroups.model.view.MemberVM;

import java.util.ArrayList;

/**
 * Created by Christian Tamakloe on 02/03/2016.
 */
public class ContactHelper {
    private static final String TAG = "DBG_ContactHelper";

    public static void saveToContacts(ContentResolver resolver, Context context, GroupVM group) {
        String groupPIN = group.getUid();
        for (MemberVM member : group.getOnlyMemberVMs()) {
            addContact(resolver, context, member, groupPIN);
        }
    }


    public static void addContact(ContentResolver resolver, Context context, MemberVM member, String groupPIN) {

        String name = groupPIN + " - " + member.getUsername();

        if (contactExists(context, name))
            return;

        try {
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

            ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                    .build());

            // username
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, name)
                    .build());

            // phoneNo
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, member.getPhoneNo())
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM)
                    .withValue(ContactsContract.CommonDataKinds.Phone.LABEL,
                            context.getResources().getString(R.string.app_name))
                    .build());

            ContentProviderResult[] results = resolver.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean contactExists(Context context, String name) {
        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = ContactsContract.Data.CONTENT_URI;
        String[] projection = new String[] { ContactsContract.PhoneLookup._ID };
        String selection = ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME + " = ?";
        String[] selectionArguments = { name };
        Cursor cursor = contentResolver.query(uri, projection, selection, selectionArguments, null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
//                return cursor.getString(0);
                return true;
            }
        }
        return false;
//        return "John Johnson not found";
    }
}
