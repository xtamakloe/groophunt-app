package com.artoconnect.whatgroups.helper;

/**
 * Created by Christian Tamakloe on 04/03/2016.
 */
public interface SessionEventListener {
    void onProcessSessionRequest(String operation, String username, String phoneNo, String password);
}
