package com.artoconnect.whatgroups.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.artoconnect.whatgroups.model.api.MemberAM;

/**
 * Created by Christian Tamakloe on 24/02/2016.
 */
public class Settings {
    private static final String PREF_KEY_MEMBER_ID = "PREF_KEY_MEMBER_ID";
    private static final String PREF_KEY_MEMBER_USERNAME = "PREF_KEY_MEMBER_USERNAME";
    private static final String PREF_KEY_MEMBER_PHONE = "PREF_KEY_MEMBER_PHONE";
    private static Settings sInstance;
    private static Context sContext;


    private Settings(Context context) {
        sContext = context;
    }


    public static Settings getInstance(Context context) {
        if (sInstance == null)
            sInstance = new Settings(context);
        return sInstance;
    }


    public SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(sContext);
    }


    public int getCurrentMemberId() {
        return getPreferences().getInt(PREF_KEY_MEMBER_ID, -1);
    }


    public void setCurrentMemberId(int memberId) {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(PREF_KEY_MEMBER_ID, memberId);
        editor.apply();
    }


    public void setCurrentMember(MemberAM member) {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(PREF_KEY_MEMBER_ID, member.getId());
        editor.putString(PREF_KEY_MEMBER_PHONE, member.getPhoneNo());
        editor.putString(PREF_KEY_MEMBER_USERNAME, member.getUsername());
        editor.apply();
    }


    public void logIn(MemberAM member) {
        setCurrentMember(member);
    }


    public void logOut() {
        SharedPreferences.Editor editor = getPreferences().edit();
        editor.putInt(PREF_KEY_MEMBER_ID, -1);
        editor.putString(PREF_KEY_MEMBER_PHONE, null);
        editor.putString(PREF_KEY_MEMBER_USERNAME, null);
        editor.apply();
    }
}
